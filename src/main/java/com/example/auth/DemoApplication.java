package com.example.auth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;

import javax.annotation.PostConstruct;
import java.util.TimeZone;


/**
 *  Configuring Spring Boot to use Java 8 Date/Time converters and UTC Timezone
 */
@SpringBootApplication
@EntityScan(basePackageClasses = {
        DemoApplication.class,
        Jsr310JpaConverters.class
})
public class DemoApplication {

    /**
     *  Configuring Spring Boot to use Java 8 Date/Time converters and UTC Timezone
     */
    @PostConstruct
    void init() {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
    }

    public static void main(String[] args) {
        SpringApplication.run(DemoApplication.class, args);
    }

}
